# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field IOC
# @runtime YES

#Load the customizing database that contains default values in case autosave files are missing
dbLoadRecords("vacs-accv_vac-ioc-11010.db")

#Load the customizing database that controls VVMC operation modes
dbLoadRecords("vacs-accv_vac-ioc-11010_vvmc.db")

#Load the customizing database for RFQ pumping group 2
dbLoadRecords("vacs-accv_vac-ioc-11010_rfq.db")

#Load iocStats records
dbLoadTemplate(iocAdminSoft.substitutions, "IOC=$(IOC=VacS-ACCV:Vac-IOC-11010)")

requireSnippet(recSyncSetup.cmd)

# Configure autosave
# Number of sequenced backup files to write
save_restoreSet_NumSeqFiles(1)

# Specify directories in which to search for request files
set_requestfile_path("$(SAVEFILE_DIR)", "reqs")

# Specify where the save files should be
set_savefile_path("$(SAVEFILE_DIR)", "")

# Specify what save files should be restored
set_pass0_restoreFile("vacs-accv_vac-ioc-11010.sav")

# Create monitor set
system("mkdir -p '$(SAVEFILE_DIR)'/reqs")
doAfterIocInit("makeAutosaveFileFromDbInfo('$(SAVEFILE_DIR)/reqs/vacs-accv_vac-ioc-11010.req', 'autosaveFields_pass0')")
doAfterIocInit("create_monitor_set('vacs-accv_vac-ioc-11010.req', 1, '')")
